
package entidades;


public class Imagen {
    private String conttype;
    private String imgB64;

    public Imagen() {
    }

    public String getConttype() {
        return conttype;
    }

    public void setConttype(String conttype) {
        this.conttype = conttype;
    }

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }
    
    
    
}
