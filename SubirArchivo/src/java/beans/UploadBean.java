
package beans;

import entidades.Imagen;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

@ManagedBean(name ="upload")
@SessionScoped
public class UploadBean implements Serializable{
    
    private Part file;
    private String imgB64;
    private String conttype;
    private List<Imagen> imagenes;
    
    public UploadBean() {
        file = null;
        imgB64 = "";
        conttype = "";
        imagenes = new ArrayList();
    }
    
    public void upload(){
        try {
            if(file!=null){
                System.out.println("img"+imgB64);
                //crud
            }
        } catch (Exception e) {
        }
    
                
    }
    
    private void getBase64(){
        try {
            InputStream is = null;
            
            if(file != null){
                conttype = file.getContentType();
                is = file.getInputStream();
                
                byte[] archivo = new byte[is.available()];
                is.read(archivo,0,archivo.length);
                
                imgB64 = DatatypeConverter.printBase64Binary(archivo);
                
                System.out.println("Img b64 = "+imgB64);
            }
            
        } catch (Exception e) {
        }
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
        getBase64();
    }

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }

    public String getConttype() {
        return conttype;
    }

    public void setConttype(String conttype) {
        this.conttype = conttype;
    }

    public List<Imagen> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<Imagen> imagenes) {
        this.imagenes = imagenes;
    }
    
    
    
    
}
