/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var bucle;

function play(){
    var reproductor =document.getElementById('audio');   
    var playBtn = document.getElementById('play');
    
    
    if(!reproductor.paused && !reproductor.ended){
        playBtn.innerHTML ='<i class="far fa-play-circle"></i>';
        reproductor.pause();
        window.clearInterval(bucle);
    }else{
        playBtn.innerHTML ='<i class="far fa-pause-circle"></i>';
        reproductor.play();
        bucle = setInterval(estado,1000);
    }
}

function stop(){
     var playBtn = document.getElementById('play');
     var reproductor =document.getElementById('audio');   
     var progreso = document.getElementById('progreso'); 
     progreso.style.width='0%';
     reproductor.currentTime=0;
     playBtn.innerHTML ='<i class="far fa-play-circle"></i>';
     reproductor.pause();
}

function estado(){
    var playBtn = document.getElementById('play');
    var reproductor =document.getElementById('audio'); 
    var progreso = document.getElementById('progreso'); 
    if(!reproductor.ended){
        var total = parseInt(reproductor.currentTime*100/reproductor.duration);
        
        progreso.style.width=total+'%';
//        console.log(reproductor.currentTime);
//        console.log(reproductor.duration);
//        console.log(total);
    }else{
        progreso.style.width='0%';
        playBtn.innerHTML ='<i class="far fa-play-circle"></i>';
    }
}

function mover(e){
    
    var progreso = document.getElementById('progreso'); 
    var reproductor =document.getElementById('audio'); 
    if(!reproductor.paused && !reproductor.ender){
        var ratonX = e.pageX-barra.offsetLeft;
        console.log(ratonX/300);
        ratonX = (ratonX/280)*100;
        var nuevoTiempo = ratonX*reproductor.duration/100;
        reproductor.currentTime=nuevoTiempo;
        progreso.style.width=ratonX+'%';
    }
}

var volumen = 1;
function mute(){
    console.log('eeee');
    var reproductor =document.getElementById('audio'); 
    var mute =document.getElementById('mute'); 
    if(reproductor.volume === 0.0){
        reproductor.volume = volumen;
        mute.innerHTML = '<i class="fas fa-volume-up"></i>';
    }else{
        volumen = reproductor.volume;
        reproductor.volume = 0;
        mute.innerHTML = '<i class="fas fa-volume-mute"></i>';
    }
}


function volumenControl(e){
    console.log('eeee');
    var vol =document.getElementById('vol'); 
    var reproductor =document.getElementById('audio'); 
        var ratonX = e.pageX-volCtrl.offsetLeft;
        console.log(ratonX);
        ratonX = (ratonX/100.0);
        reproductor.volume = ratonX;
        vol.style.width = (ratonX*100)+'px';
}