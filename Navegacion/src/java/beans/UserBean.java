
package beans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="user")
@RequestScoped
public class UserBean implements Serializable{
    
    private boolean logear = false;
    
    public String login(){
        if(this.logear){
            return "sucess";
        }else{
            return "fail";
        }
    }

    public UserBean() {
    }

    public boolean isLogear() {
        return logear;
    }

    public void setLogear(boolean logear) {
        this.logear = logear;
    }
    
    
    
}
