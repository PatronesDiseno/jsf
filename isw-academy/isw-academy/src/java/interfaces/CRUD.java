
package interfaces;

import java.util.List;


public interface CRUD<T> {
    public boolean crear(T t);
    public boolean editar(T t);
    public boolean eliminar(int id);
    public List<T> obtenerTodos();
    public T obtenerId(int id);
}
