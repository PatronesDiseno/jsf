
package interfaces;

import entidades.Categoria;
import java.util.List;

public interface ICategoriaDAO extends CRUD<Categoria>{
    public List<Categoria> buscarPor(String nombre);
}
