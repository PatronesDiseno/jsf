package interfaces;

import entidades.User;


public interface IUsuarioDAO extends CRUD<User>{
    public User login(User usuario);
}
