
package beans;

import configuracion.DBHelper;
import configuracion.Seguridad;
import entidades.User;
import implementacion.UsuarioDAOImp;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ManagedBean(name="user")
@SessionScoped
public class UserBean implements Serializable {
    private User usuario;
    private String error;
    private UsuarioDAOImp DAO;
    private boolean rememberme = false;

    public UserBean() {
        usuario = new User();
        error = "";
        DAO = new UsuarioDAOImp();
    }
    
    public String login(){
        
        if(usuario.getUsername() != null 
        && !usuario.getUsername().equals("")
        && usuario.getPassword() != null
        && !usuario.getPassword().equals("")
        ){
            this.usuario = DAO.login(usuario);
            if(this.usuario != null && (DAO.getError() == null || DAO.getError().isEmpty())){
                if(this.rememberme){
                    guardarCookie();
                }

                return "bienvenida";
            }else{
                this.error = DAO.getError();
            }
            
        }
        
        return "index";
    }
    
    public void loadCookie(){
        Cookie cookie = getCookie("rememberme");
        if(cookie != null){
            try {
                FacesContext fc = FacesContext.getCurrentInstance();
                String user[] = Seguridad.decrypt(cookie.getValue()).split(":");
                
                this.usuario.setId(Integer.valueOf(user[0]));
                this.usuario.setFullname(user[1]);
                this.usuario.setEmail(user[2]);
                this.usuario.setUsername(user[3]);
                
                fc.getApplication()
                        .getNavigationHandler().handleNavigation(fc, null, "bienvenida");
                
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
        }
    }
    
    private void guardarCookie(){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) 
                    fc.getExternalContext().getRequest();
            
            Cookie cookie = null;
            Cookie userCookies[] = request.getCookies();
            if(userCookies != null && userCookies.length > 0){
                for(Cookie ck:userCookies){
                    if(ck.getName().equals("rememberme")){
                        cookie = ck;
                        break;
                    }
                }
            }
            
            //ID:Fullname:Email:username
            String valor = this.usuario.getId()+":"+this.usuario.getFullname()+":";
                   valor+= this.usuario.getEmail()+":"+this.usuario.getUsername();
                   
            //valor encriptado
            valor = Seguridad.encrypt(valor); //encryptado con base64
            if(cookie != null){//existe
                cookie.setValue(valor);
            }else{
                cookie = new Cookie("rememberme",valor);
                cookie.setPath(request.getContextPath());
            }
            
            cookie.setMaxAge(3600);
            HttpServletResponse response = (HttpServletResponse)
                                    fc.getExternalContext().getResponse();
            response.addCookie(cookie);
            
        } catch (Exception e) {
        }
    }
    
    public Cookie getCookie(String name){
        try {
             FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) 
                    fc.getExternalContext().getRequest();
            
            Cookie cookie = null;
            Cookie userCookies[] = request.getCookies();
            if(userCookies != null && userCookies.length > 0){
                for(Cookie ck:userCookies){
                    if(ck.getName().equals("rememberme")){
                        cookie = ck;
                        return cookie;
                    }
                }
            }
            
        } catch (Exception e) {
        }
        return null;
    }

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

    public String getError() {
        return error;
    }

    public boolean isRememberme() {
        return rememberme;
    }

    public void setRememberme(boolean rememberme) {
        this.rememberme = rememberme;
    }
    
    
}
