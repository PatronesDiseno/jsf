package beans;

import entidades.Curso;
import implementacion.CursoDAOImp;
import interfaces.ICursoDAO;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

@ManagedBean(name="curso")
@RequestScoped
public class CursoBean {
    private Curso curso = new Curso();
    private Part file;
    private String conttype;
    private String imgB64;
    private CursoDAOImp dao;

    public CursoBean() {
        file = null;
        dao = new CursoDAOImp();
    }
    
    public void agregar(){
        if(imgB64!=null && !imgB64.isEmpty()){ // png jpg gif bmp  //mp4 mpeg wav 
            Date fechaCreacion = new Date();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
//            String fechaC  = "";  //24-12-2019
//             fechaC += fechaCreacion.getYear()+"-";
//             fechaC += fechaCreacion.getMonth()+"-";
//             fechaC += fechaCreacion.getDate() +"-";
//            System.out.println("Fecha creacion = "+fechaCreacion);
            System.out.println("Fecha creacion formato = "
                    + formato.format(fechaCreacion) );
            //recibir campos
            curso.setFechaCreacion(formato.format(fechaCreacion));
            curso.setFechaModificacion(formato.format(fechaCreacion));
            curso.setConttype(conttype);
            curso.setImagen(imgB64);
            curso.setIdCategoria(0); //momentaneo 
            curso.setIdUser(0); //momentaneo
            
            dao.crear(curso);
        }
    }
    
    private void getBase64(){
        try {
            InputStream is = null;            
            if(file != null){
                conttype = file.getContentType();
//                file.getSize()
                is = file.getInputStream();                
                byte[] archivo = new byte[is.available()];
                is.read(archivo,0,archivo.length);                
                imgB64 = DatatypeConverter.printBase64Binary(archivo);                
                System.out.println("Img b64 = "+imgB64);
            }
            
        } catch (Exception e) {
        }
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
        getBase64();
    }
    
    
    
    
}
