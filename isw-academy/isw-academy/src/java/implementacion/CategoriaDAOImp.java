
package implementacion;

import configuracion.DBHelper;
import entidades.Categoria;
import interfaces.ICategoriaDAO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class CategoriaDAOImp implements ICategoriaDAO{

    DBHelper db = new DBHelper();
    
    @Override
    public List<Categoria> buscarPor(String nombre) {
        List<Categoria> categorias = new ArrayList();
        
        try{
            if(db.connect()){
                String query ="SELECT * FROM Categoria WHERE "
                        + " nombre LIKE '%"+nombre+"%'";
                
                ResultSet result = (ResultSet) db.execute(query, false);
                
                while(result.next()){
                    Categoria categoria = new Categoria();
                    categoria.setId(result.getInt("id"));
                    categoria.setNombre(result.getString("nombre"));
                    categoria.setDescripcion(result.getString("descripcion"));
                    categorias.add(categoria);
                }
                
            }
        }catch (Exception ex){
            
        }finally{
            db.disconnect();
        }
        
        return categorias;
    }

    @Override
    public boolean crear(Categoria t) {
        boolean resultado = false;
        
        try{
            if(db.connect()){
                String query ="INSERT INTO Categoria (nombre,descripcion) VALUES ("
                        + "'"+t.getNombre()+"','"+t.getDescripcion()+"')";
                resultado = (boolean) db.execute(query, true);
            }
        }catch (Exception ex){
            
        }finally{
            db.disconnect();
        }
        
        return resultado;
    }

    @Override
    public boolean editar(Categoria t) {
        boolean resultado = false;
        
        try{
            if(db.connect()){                
                String query ="UPDATE Categoria SET "
                        + " nombre = '"+t.getNombre()+"',descripcion = '"+t.getDescripcion()+"' WHERE "
                        + " id = "+t.getId()+"";
                resultado = (boolean) db.execute(query, true);
            }
        }catch (Exception ex){
            
        }finally{
            db.disconnect();
        }
        
        return resultado;
   
    }

    @Override
    public boolean eliminar(int id) {
        boolean resultado = false;
        
        try{
            if(db.connect()){
                String query ="DELETE FROM Categoria  WHERE  id = "+id+"";
                resultado = (boolean) db.execute(query, true);
            }
        }catch (Exception ex){
            
        }finally{
            db.disconnect();
        }
        
        return resultado;
    
    }

    @Override
    public List<Categoria> obtenerTodos() {
        List<Categoria> categorias = new ArrayList();
        
        try{
            if(db.connect()){
                String query ="SELECT * FROM Categoria";
                
                ResultSet result = (ResultSet) db.execute(query, false);
                
                while(result.next()){
                    Categoria categoria = new Categoria();
                    categoria.setId(result.getInt("id"));
                    categoria.setNombre(result.getString("nombre"));
                    categoria.setDescripcion(result.getString("descripcion"));
                    categorias.add(categoria);
                }
            }
        }catch (Exception ex){
            
        }finally{
            db.disconnect();
        }
        
        return categorias;
    
    }

    @Override
    public Categoria obtenerId(int id) {
        Categoria categoria = new Categoria();
        
        try{
            if(db.connect()){
                String query ="SELECT * FROM Categoria WHERE "
                        + " id = "+id+"";
                
                ResultSet result = (ResultSet) db.execute(query, false);
                
                if(result.next()){
                    categoria.setId(result.getInt("id"));
                    categoria.setNombre(result.getString("nombre"));
                    categoria.setDescripcion(result.getString("descripcion"));
                }
            }
        }catch (Exception ex){
            
        }finally{
            db.disconnect();
        }
        
        return categoria;
    }
    
}
