
package implementacion;

import configuracion.DBHelper;
import entidades.Curso;
import interfaces.ICursoDAO;
import java.util.List;

public class CursoDAOImp implements ICursoDAO{
    
    DBHelper db = new DBHelper();
    
    @Override
    public boolean crear(Curso curso) {
        boolean resultado = false;
        
        try {
            String query = "INSERT INTO cursos (idUser,idCategoria,"
                    + "nombre,descripcion,precio,imagen,fechaCreacion"
                    + ",fechaModificacion,conttype) VALUES ("
                    + ""+curso.getIdUser()+","
                    + ""+curso.getIdCategoria()+","
                    + "'"+curso.getNombre()+"',"
                    + "'"+curso.getDescripcion()+"',"
                    + ""+curso.getPrecio()+","
                    + "'"+curso.getImagen()+"',"
                    + "'"+curso.getFechaCreacion()+"',"
                    + "'"+curso.getFechaModificacion()+"',"
                    + "'"+curso.getConttype()+"'"
                    + ")";
            if(db.connect()){
                resultado = (boolean) db.execute(query, true);
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        
        
        return resultado;
    }

    @Override
    public boolean editar(Curso t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Curso> obtenerTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Curso obtenerId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
