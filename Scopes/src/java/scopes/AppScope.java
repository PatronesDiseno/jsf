
package scopes;

import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="app")
@ApplicationScoped
public class AppScope implements Serializable{
    private int contador = 0;
    
    public String clickMe(){
        contador++;
        return "index.xhtml";
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }
    
    
}
